'''@file fibonacci_louie.py
   This file accepts input from the user and prints a Fibonacci number at a specified
   index.
'''
def fib (idx):
    '''
    @brief     This function calculates a Fibonacci number at a specified index.
    @param idx An integer specifying the index of the desired Fibonacci number
    '''
    f_n_minus_2=0                               #define value for index 0
    f_n_minus_1=1                               #define value for index 1

    if idx == 0:
        return f_n_minus_2                      #return for index 0
    elif idx == 1:
        return f_n_minus_1                      #return for index 1
    else:
        for i in range(1, idx):                 #repeat while i between 0 and requested index
            f_n = f_n_minus_2 + f_n_minus_1     #find f_n by adding 2 previous terms
            f_n_minus_2 = f_n_minus_1           #update vars with new values
            f_n_minus_1 = f_n                   #update vars with new values
    return f_n

if __name__ == '__main__':
    while True:
        try:        
            idx = int(input('Choose an index: '))                    #recieve user input for index
        except ValueError:
            print("The index must be an integer.")                   #if input cannot be converted to integer, give error message and return to beginning of loop
            continue
        if idx < 0:
            print("The index must be nonnegative.")                  #if input is negative, return to beginning of loop
            continue
        print ('Fibonacci number at '                   
               'index {:} is {:}.'.format(idx,fib(idx)))             #print fib number at index
        x = input('Enter q to quit or press enter to continue: ')    #recieve user input for continue/quit   
        if x == "q":                            
            break                                                    #break if q input
        elif x == "":
            pass                                                     #continue if enter input