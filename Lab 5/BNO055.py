''' @file       BNO055.py
    @brief      IMU driver to collect data and perform other functions on BNO055
    @details    IMU driver provides functions to change operating mode, calibration, and collecting angle and velocity values in degrees
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 10, 2021
    @copyright  CC BY
'''

from pyb import I2C
import struct
import utime

class BNO055:
    
    def __init__(self):
        
        ''' @brief              Sets up i2c and defines variables
            @details            Sets up i2c in master mode and create buffer variables to store data
        '''
        
        ## @brief    I2c variable 
        #  @details  Sets i2c object in master mode
        #
        self.i2c = I2C(1, I2C.MASTER)
        
        ## @brief     Radius buffer
        #  @details   2 byte buffer to store radius values
        #
        self.buf_radius = bytearray(2)
        
        ## @brief     Buffer
        #  @details   6 byte buffer to store angle and velocity values
        #
        self.buf = bytearray(6)
        
        ## @brief     Calibration values
        #  @details   8 byte bytearray to store calibration status
        #
        self.cal_bytes = bytearray(8)

    def operating_mode(self, mode):
        ''' @brief              Sets operating mode
            @details            Sets operating mode of sensor by writing to register 0x3D
        '''
        self.i2c.mem_write(mode, 0x28, 0x3D)     #writes mode to 0x3D, the operating mode register
        
    def set_units(self, units):
        ''' @brief              Sets units
            @details            Sets units of sensor by writing to register 0x3B
        '''
        self.i2c.mem_write(units, 0x28, 0x3B)    #writes units to 0x3B, the units register

    def calib_status(self):
        ''' @brief              Gets calibration status of sensor and prints it
            @details            Reads calibration status and stores in 8 byte bytearray
            @return             Returns calibrated boolean
        '''
        self.i2c.mem_read(self.cal_bytes, 0x28, 0x35)
        
        ## @brief     Calibration status
        #  @details   Calibration status of sensor, 3 is fully calibrated
        #
        
        cal_status = ( self.cal_bytes[0] & 0b11,
              (self.cal_bytes[0] & 0b11 << 2) >> 2,
              (self.cal_bytes[0] & 0b11 << 4) >> 4,
              (self.cal_bytes[0] & 0b11 << 6) >> 6)     #bit shifting to read data

        if(cal_status[0] == 3 and cal_status[1] == 3 and cal_status[2] == 3 and cal_status[3] == 3):
        #sets calibrated to true if all values are 3
            
        ## @brief     Calibrated flag
        #  @details   Variable set to true when sensor is calibrated and false when sensor is not calibrated
        #
            calibrated = True
        else:
            calibrated = False
            print('Waiting for sensor calibration') 
            print("Calibration Status:", cal_status)
        return calibrated
        
    def euler_angle(self, calibrated):
        ''' @brief              Gets euler angles
            @details            Gets euler angles by reading 0x1A and storing in 6 byte bytearray to be unpacked into a tuple and printed          
        '''
        if(calibrated):
            self.i2c.mem_read(self.buf, 0x28, 0x1A)     #reads 0x1A, the euler angle registers
            
            ## @brief     Angle signed ints
            #  @details   Unpacked angle values from buffer
            #
            angle_signed_ints = struct.unpack('<hhh', self.buf)
            
            ## @brief     Angle values
            #  @details   Angle values in a tuple containing heading, roll, and pitch
            #
            angle_vals = tuple(angle_int/16 for angle_int in angle_signed_ints)
            
            print('Angles: (Heading, Roll, Pitch)',angle_vals)
        
        
    def euler_velocity(self, calibrated):
        ''' @brief              Gets euler velocities
            @details            Gets euler velocities by reading 0x14 and storing in 6 byte bytearray to be unpacked into a tuple and printed            
        '''
        if(calibrated):
            self.i2c.mem_read(self.buf, 0x28, 0x14)
            
            ## @brief     Velocity signed ints
            #  @details   Unpacked velocity values from buffer
            #
            vel_signed_ints = struct.unpack('<hhh', self.buf)   #unpacks buffer
            
            ## @brief     Velocity values
            #  @details   Velocity values in a tuple, containing X, Y, and Z
            #
            vel_vals = tuple(vel_int/16 for vel_int in vel_signed_ints)
            
            print('Velocity: (X, Y, Z)',vel_vals)
            
    def calib_read(self, sensor):
        ''' @brief              Gets calibration coefficients
            @details            Gets calibration coefficients for accelerometer, gyroscope, or magnetometer depending on sensor parameter
            @param              sensor specifies which sensor to read calibration coefficients for
            @return             Returns calibration coefficients of the specified sensor
        '''

        if(calibrated):
            self.i2c.mem_write(0, 0x28, 0x3D)

            if(sensor == 'acc'):
                self.i2c.mem_read(self.buf, 0x28, 0x55)
                self.i2c.mem_read(self.buf_radius, 0x28, 0x67)
                
                ## @brief     Accelerometer calibration signed ints
                #  @details   Unpacked accelerometer calibration coefficients from buffer
                #
                acc_signed_ints = struct.unpack('<hhh', self.buf)
                
                ## @brief     Accelerometer calibration values
                #  @details   Accelerometer calibration values in a tuple
                #
                acc_vals = tuple(acc_int for acc_int in acc_signed_ints)
                
                ## @brief     Accelerometer radius signed ints
                #  @details   Unpacked accelerometer calibration coefficients from buffer
                #
                acc_radius_signed_ints = struct.unpack('<h', self.buf_radius)
                
                ## @brief     Accelerometer buffer
                #  @details   Buffer to store accelerometer calibration values for use in write function
                #
                acc_buf = self.buf
                
                ## @brief     Accelerometer radius buffer
                #  @details   Buffer to store accelerometer radius values for use in write function
                #
                acc_radius_buf = self.buf_radius
                
                print('Accelerometer Calibration Values:', acc_vals)
                print('Accelerometer Radius Values:', acc_radius_signed_ints[0])
                
                return acc_buf, acc_radius_buf
                
            if(sensor == 'mag'):
                self.i2c.mem_read(self.buf, 0x28, 0x5B)
                self.i2c.mem_read(self.buf_radius, 0x28, 0x69)
                
                ## @brief     Magnetometer calibration signed ints
                #  @details   Unpacked magnetometer calibration coefficients from buffer
                #
                mag_signed_ints = struct.unpack('<hhh', self.buf)
                
                ## @brief     Magnetometer calibration values
                #  @details   Magnetometer calibration values in a tuple
                #
                mag_vals = tuple(mag_int for mag_int in mag_signed_ints)
                
                ## @brief     Magnetometer radius signed ints
                #  @details   Unpacked magnetometer calibration coefficients from buffer
                #
                mag_radius_signed_ints = struct.unpack('<h', self.buf_radius)
                
                ## @brief     Magnetometer buffer
                #  @details   Buffer to store magnetometer calibration values for use in write function
                #
                mag_buf = self.buf
                
                ## @brief     Magnetometer radius buffer
                #  @details   Buffer to store magnetometer radius values for use in write function
                #
                mag_radius_buf = self.buf_radius

                
                print('Magnetometer Calibration Values:', mag_vals)
                print('Magnetometer Radius Values:', mag_radius_signed_ints[0])
                
                return mag_buf, mag_radius_buf
                
            if(sensor == 'gyr'):
                self.i2c.mem_read(self.buf, 0x28, 0x61)
                
                ## @brief     Gyroscope calibration signed ints
                #  @details   Unpacked gyroscope calibration coefficients from buffer
                #
                gyr_signed_ints = struct.unpack('<hhh', self.buf)
                
                ## @brief     Gyroscope calibration values
                #  @details   Gyroscope calibration values in a tuple
                #
                gyr_vals = tuple(gyr_int for gyr_int in gyr_signed_ints)
                
                ## @brief     Gyroscope buffer
                #  @details   Buffer to store gyroscope calibration values for use in write function
                #
                gyr_buf = self.buf
                
                print('Gyroscope Calibration Values:', gyr_vals)
                
                return gyr_buf
        else:
            print('Waiting for calibration')
    
    def calib_write(self, sensor, acc_buf, acc_radius_buf, mag_buf, mag_radius_buf, gyr_buf):
        ''' @brief              Writes calibration coefficients
            @details            Writes calibration coefficients stored from calib_read function depending on specified sensor
            @param              sensor parameter determines which sensor to write to
            @return             
        '''
        self.i2c.mem_write(0, 0x28, 0x3D)
        if(sensor == 'acc'):
            self.i2c.mem_write(acc_buf, 0x28, 0x55)     #writes stored acceleration buf to 0x55
            self.i2c.mem_write(acc_radius_buf, 0x28, 0x67)  #writes stored acceleration radius to 0x67
            
        if(sensor == 'mag'):
            self.i2c.mem_write(mag_buf, 0x28, 0x5B)
            self.i2c.mem_write(mag_radius_buf, 0x28, 0x69)
        
        if(sensor == 'gyr'):
            self.i2c.mem_write(gyr_buf, 0x28, 0x61)
        
        
if __name__ == '__main__':
    BNO055().operating_mode(12) #sets operating mode to NDOF

    
    while True:
        calibrated = BNO055().calib_status()
        BNO055().euler_angle(calibrated)
        BNO055().euler_velocity(calibrated)
        #BNO055().calib_read('acc')
        #BNO055().calib_read('mag')
        #BNO055().calib_read('gyr')
        print('\n')
        BNO055().operating_mode(12)
        utime.sleep(0.5)