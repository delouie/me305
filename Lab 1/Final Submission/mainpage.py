# -*- coding: utf-8 -*-

'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py

   @mainpage

   @section sec_intro   Introduction
                        Introduction to Hardware: Lab 0x01. This program controls an LED. We simulate a square wave, sine wave, and saw tooth pattern for each LED stage. Users are prompted to push a button to cycle through each LED pattern.
                        \image html statemachine.png 
                        
                        Watch a demonstration of our LED setup here: 
                            
                        \htmlonly
                        <iframe width="560" height="315" src="https://youtu.be/9s8IKGPuduw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        
   @author              Jameson Spitz
   @author              Derick Louie

   @copyright           CC BY

   @date                October 8, 2021
'''