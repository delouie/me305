# -*- coding: utf-8 -*-

''' @file
    @brief This file cycles through LED patterns on a nucleo based on button presses.
    @details The LED cycles through a square wave light, sine wave light, and a saw tooth light pattern. The user is prompted to push the button through REPL. The current pattern is printed to the console at each stage.
    @author Derick Louie
    @author Jameson Spitz
    @date October 8, 2021
    @copyright CC BY
'''

# The next state to run as the FSM iterates
global state   
import pyb
import math
import time

## @brief Pin associated with LED
#  @details Identifies this variable is connected to the pin A5 on the nucleo which controls the LED.
    
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

## @brief PWM timer
#  @details Sets up timer 2 at a frequency of 20 kHz.
    
tim2 = pyb.Timer(2, freq = 20000)

## @brief Associate tim2 to channel 1.
#  @details Configures channel 1 to perform PWM.
    
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)

## @brief Pin associated with button
#  @details Identifies C13 as the pin for the button used for user input.
    
pinC13 = pyb.Pin(pyb.Pin.cpu.C13)

def onButtonPressFCN(IRQ_src):
    ''' @brief This function is an interrupt that is used when button is pressed.
        @details Sets the condition that buttonpressed is true and increments the state by 1.
    '''
    
    ## @brief Boolean associated with button being pressed.
    #  @details Toggled when button is pressed to advance states in the main task.
    
    global buttonpressed
    
    ## @brief Current state system is in.
    #  @details Defines which block in main task should be run.
    
    state = 0
    state += 1
    buttonpressed = True
  
## @brief Creates interrupt when button is pressed.
#  @details Calls interrupt using pyb.ExtInt.

ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
# Creating main script

if __name__ == '__main__':
    
    buttonpressed = False
    state = 0
    
    print('Program start. Push button B1 to start program.')

    while(True):
        try:
            if (state == 0):               # Run state 0 code
                if(buttonpressed == True):
                    state = 1              # Transition to state 1
                    print('Program started. Push button B1 for square wave pattern.')
                    buttonpressed = False  # Reset buttonpressed variable
            
            elif (state == 1):             # Run state 1 code
                if(buttonpressed == True):
                    state = 2              # Transition to state 2
                    buttonpressed = False  # Reset buttonpressed variable
            
            elif (state == 2):             # Run state 2 code
                print('Square wave pattern started. Push button B1 for sine wave pattern.')
                
                ## @brief Represents current time.
                #  @details Stores time in order to reset LED pattern timer at start of each pattern.
                
                starttime=time.ticks_ms()

                while(True):
                    
                    ## @brief Represents time since last timer reset.
                    #  @details Stores current time subtracted by starttime to obtain time elapsed since pattern start.
                    
                    timeelapsed=time.ticks_ms()-starttime     
                
                    if(timeelapsed in range(0,500)):
                        t2ch1.pulse_width_percent(100)       # Between 0 and 0.5 seconds turn the light on.
                    elif(timeelapsed in range(500,1000)):    
                        t2ch1.pulse_width_percent(0)         # Between 0.5 ans 1 second turn the light off.
                    else:
                        starttime=time.ticks_ms()            # Resets timer so that it doesn't exceed 1 second.
                
                    if(buttonpressed == True):
                        state = 3                            # Transition to state 3
                        buttonpressed = False                # Reset buttonpressed variable
                        break
                    
            elif (state == 3):                               # Run state 3 code
                print('Sine wave pattern started. Push button B1 for sawtooth pattern.')
                starttime=time.ticks_ms()                    # Resets clock to begin at new time cycle

                while(True):
                    timeelapsed=time.ticks_ms()-starttime    
                
                    if(timeelapsed in range(0,10000)):       # Between 0 and 10 seconds alter the LED brightness in a sine wave pattern.
                        t2ch1.pulse_width_percent(math.sin(2*math.pi/10000*timeelapsed)*50 + 50)
                    else:
                        starttime=time.ticks_ms()            # Resets timer after 10 seconds
                    if(buttonpressed == True):
                        state = 4                            # Transition to state 4
                        buttonpressed = False                # Reset buttonpressed variable
                        break
                    
            elif (state == 4):
                # Run state 4 code
                print('Sawtooth pattern started. Push button B1 for square wave pattern.')
                starttime=time.ticks_ms()

                while(True):
                    timeelapsed=time.ticks_ms()-starttime
                
                    if(timeelapsed in range(0,1000)):        # Between 0 and 1 second alter LED brightness in sawtooth pattern.
                        t2ch1.pulse_width_percent(timeelapsed/10)
                    else:
                        starttime=time.ticks_ms()            # Resets timer after 1 second.
                        
                    if(buttonpressed == True):
                        buttonpressed = False                # Reset buttonpressed variable
                        state = 2                            # Transition to state 2
                        break
            
        except KeyboardInterrupt:
            break                      # Clean up break with ctrl + c
        
    print('Program Terminating')       # Exiting the program and cleaning up   
        
