''' @file       encoder.py
    @brief      Encoder driver to handle overflow and encoder commands
    @details    Encoder drive handles overflow and creates update, delta, and position commands
    @author     Jameson Spitz
    @author     Derick Louie
    @date       October 21, 2021
    @copyright  CC BY
'''

import pyb

class Encoder:
    
    ''' @brief     Encoder Driver
        @details   Works directly with the Nucleo hardware. Tracks the position of the motor, adjusting for the overflow of our period.
                   Can return position, set position, and return the chage in position.
    '''
    
    def __init__(self):
        
        ''' @brief              Sets up encoder pins and corresponding timers
            @details            Sets up pins PB6 and PB7 for encoder, sets up timers, and defines variables
        '''
        
        ## @brief    Pin PB6 associated with encoder
        #  @details  Identifies this variable is connected to pin PB6 on the nucleo
        #
        
        pinPB6 = pyb.Pin (pyb.Pin.board.PB6, pyb.Pin.IN)
        
        ## @brief    Pin PB7 associated with encoder
        #  @details  Identifies this variable is connected to pin PB7 on the nucleo
        #
        
        pinPB7 = pyb.Pin (pyb.Pin.board.PB7, pyb.Pin.IN)
        
        ## @brief    Timer 4
        #  @details  Sets up timer 4 with period of 65535 and prescaler of 0
        #
        
        self.tim4 = pyb.Timer(4, period = 65535, prescaler = 0)
        
        ## @brief    Timer 4 Channel 1
        #  @details  Configures timer 4 on channel 1
        #
        
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_A, pin=pinPB6)
        
        ## @brief    Timer 4 Channel 2
        #  @details  Configures timer 4 on channel 2
        #
        
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_A, pin=pinPB7)
        
        ## @brief    Old tick value to store previous encoder value
        #  @details  Variable used in update function to store previous value
        #
        
        self.old_tick=0
        
        ## @brief    Period value
        #  @details  Variable used to define overflow value for 16 bit integer
        #
        
        self.per=65535
        
        ## @brief    Position value
        #  @details  Variable used to store encoder position
        #
        
        self.position=0

        #print('Creating encoder object')
        
    def update(self):
        
        ''' @brief              Updates encoder value
            @details            Updates encoder value by adding delta to position. Handles overflows by adding or subtracting the period from delta when delta exceeds half the period
        '''
        
        new_tick=self.tim4.counter()
        self.delta=new_tick-self.old_tick
        self.old_tick=new_tick
        
        if(abs(self.delta) >= self.per/2):    #handle overflow if delta is greater than half the period
            if(self.delta > 0):
                self.delta -= self.per        #if delta is positive, subtract period
            else:
                self.delta += self.per        #if delta is negative, add period

        self.position += self.delta
            
        #print('Reading encoder count and updating position and delta values')
        
    def get_position(self):
        
        ''' @brief              Gets current encoder position
            @details            Gets current encoder position by returning position variable
            @return             Returns current encoder position
        '''
        
        return self.position
    
    def set_position(self,position):
        
        ''' @brief              Sets position of encoder
            @details            Sets position of encoder by taking input and setting position variable
            @param position     Position to set position variable to       
        '''
        
        self.position = position
        #print('Setting position and delta values')
        
    def get_delta(self):
        
        ''' @brief              Gets current delta value
            @details            Gets current delta value by returning delta variable
            @return             Returns current delta value
        '''
        return self.delta