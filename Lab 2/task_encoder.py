''' @file        task_encoder.py
    @brief       Interacts with encoder driver to perform encoder actions.
    @details     Zeros out motor position, gets position, gets delta, collects data for 30 seconds, and interrupts data collection.
    @author      Jameson Spitz
    @author      Derick Louie
    @date        October 21, 2021
    @copyright   CC BY
'''
import time
import encoder
import pyb

class Task_encoder():
    
    ''' @brief      Encoder task.
        @details    Implements finite state machine for encoder actions.
    '''
    
    def __init__(self, state, user, reset, runs):
        
        ''' @brief              Constructs an encoder task.
            @details            Defines variables and initializes encoder task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
        '''
        
        ## @brief      Instantiates connection between keyboard and Necleo.
        #  @details    Used to interrupt data collection when 's' is pressed.
        
        self.ser = pyb.USB_VCP()
        
        ## @brief      Instantiates encoder object.
        #  @details    Task encoder runs tasks through my_encoder methods.
        
        self.my_encoder = encoder.Encoder()
        self.my_state = state
        self.reset = reset
        
        ## @brief      List of data positions
        #  @details    Stores motor position for specified time or until collection is terminated.
        
        self.datacollection=[]
        
        ## @brief    Time elapsed variable.
        #  @details  Variable used to store time elapsed since timer reset.

        self.timeelapsed=0
        
        ## @brief    Start time variable.
        #  @details  Variable used to store start time for first timer for time elapsed calculation.
        
        self.starttime=time.ticks_ms()
        
        ## @brief    Start time counter variable.
        #  @details  Variable used to store start time for second timer to collect data at a certain interval.

        self.starttimec=time.ticks_ms()
        
        self.action = user
        self.runs = runs
    
    def run(self):
        
        ''' @brief              Runs one iteration of FSM
            @details            Performs encoder actions based on state variable
        '''

        if(self.reset.read() == 1):
            self.starttime=time.ticks_ms()
            self.starttimec=time.ticks_ms()
            self.timeelapsed=0
            self.timeelapsedc=0
            self.reset.write(0)
        
        if(self.ser.any()):
            char = self.ser.read(1).decode()
            if(char == 's'):
                self.action.write(0)
                self.runs.write(0)
                print(self.datacollection)  
                self.datacollection=[]

            
        self.my_encoder.update()
        
        if(self.my_state.read() == 'z'):
            self.my_encoder.set_position(0)
            print('Position Reset')
            print(self.my_encoder.get_position())
            self.action.write(0)
            
        elif(self.my_state.read() == 'p'):
            print('Current Position:',self.my_encoder.get_position())
            self.action.write(0)
            
        elif(self.my_state.read() == 'd'):
            print('Delta:', self.my_encoder.get_delta())
            self.action.write(0)
        
        elif(self.my_state.read() == 'g'):
            
            if(self.timeelapsed < 30000):
                
                self.timeelapsed=time.ticks_ms()-self.starttime
                
                self.timeelapsedc=time.ticks_ms()-self.starttimec
                
                self.my_encoder.update()
            
                if(self.timeelapsedc >= 100):
                    self.datacollection.append(self.my_encoder.get_position())
                    self.starttimec=time.ticks_ms()
            else:
                print(self.datacollection)
                self.datacollection=[]
                self.action.write(0)
                self.runs.write(0)
            
        
        