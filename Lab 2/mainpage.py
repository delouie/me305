# -*- coding: utf-8 -*-

'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py

   @mainpage

   @section sec_intro1  Lab 1
                        Lab 0x01: Introduction to Hardware. This program controls an LED. We simulate a square wave, sine wave, and saw tooth pattern for each LED stage. Users are prompted to push a button to cycle through each LED pattern.
                        \image html statemachine.png 
                        
                        Watch a demonstration of our LED setup here: 
                            
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/9s8IKGPuduw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        
   @section sec_intro2  Lab 2
                        Lab 0x02: Incremental Encoders. This program reads encoder values and prompts the user for an input. Depending on the input, the program can zero the encoder position, print the position, print delta, or collect encoder data.
                        \image html statemachine2.png 
                        
   @author              Jameson Spitz
   @author              Derick Louie

   @copyright           CC BY

   @date                October 21, 2021
'''