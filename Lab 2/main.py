''' @file       main.py
    @brief      Main program file 
    @details    Main file runs tasks set up in other files
    @author     Jameson Spitz
    @author     Derick Louie
    @date       October 21, 2021
    @copyright  CC BY
'''

import task_encoder
import task_user
import shares

def main():

    ''' @brief              Main function for setting up shared variables and running tasks
        @details            Instantiates shared variables and tasks, then runs each task in a while loop
    '''
    ## @brief    Shared state variable
    #  @details  Variable defining FSM state
    #
    
    state = shares.Share(0)
    
    ## @brief    Shared user_state variable
    #  @details  Variable defining user input
    #
    
    user_state = shares.Share(0)
    
    ## @brief    Shared reset variable
    #  @details  Variable used to reset timers
    #
    
    reset = shares.Share(0)
    
    ## @brief    Shared reset variable
    #  @details  Variable used to print on the first run of the FSM
    #   
    
    runs = shares.Share(0)
    
    ## @brief    Variable for first task
    #  @details  Instantiates user task
    #   
    
    task1 = task_user.Task_user(state,user_state,reset,runs)

    ## @brief    Variable for second task
    #  @details  Instantiates encoder task
    #   
    
    task2 = task_encoder.Task_encoder(state,user_state,reset,runs)
    
    while(True):
            task1.run()
            task2.run()
    
if __name__ == '__main__':
    main()