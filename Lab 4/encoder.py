''' @file       encoder.py
    @brief      Encoder driver to handle overflow and encoder commands
    @details    Encoder drive handles overflow and creates update, delta, and position commands
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 17, 2021
    @copyright  CC BY
'''

import pyb

class Encoder:
    
    ''' @brief     Encoder Driver
        @details   Works directly with the Nucleo hardware. Tracks the position of the motor, adjusting for the overflow of our period.
                   Can return position, set position, and return the chage in position.
    '''
    
    def __init__(self, pin1, pin2, tim, encoder_num):
        
        ''' @brief              Sets up encoder pins and corresponding timers
            @details            Sets up pins PB6 and PB7 for encoder, sets up timers, and defines variables
        '''
        
        ## @brief    Stores the first pin specified with encoder object
        #  @details  Is PB6 for encoder 1 and PC6 for encoder 2
        #
        
        self.pin1 = pin1
        
         ## @brief    Stores the second pin specified with encoder object
        #  @details  Is PB7 for encoder 1 and PC7 for encoder 2
        #
        
        self.pin2 = pin2
        
        ## @brief    Assigns pin type for pin1
        #  @details  Sets the specified pin to an In pin on the nucleo
        #
        
        pin_one = pyb.Pin (pin1, pyb.Pin.IN)
        
        ## @brief    Assigns pin type for pin2
        #  @details  Sets the specified pin to an In pin on the nucleo
        #
        
        pin_two = pyb.Pin (pin2, pyb.Pin.IN)
        
        ## @brief    Timer setup
        #  @details  Sets up timer with period of 65535 and prescaler of 0 for encoder object
        #
        
        self.tim4 = pyb.Timer(tim, period = 65535, prescaler = 0)
        
        ## @brief    Channel 1 setup
        #  @details  Configures timer on channel 1 for pin 1
        #
        
        self.t4ch1 = self.tim4.channel(1, encoder_num, pin=pin_one)
        
        ## @brief    Channel 2 setup
        #  @details  Configures timer on channel 2 for pin 2
        #
        
        self.t4ch2 = self.tim4.channel(2, encoder_num, pin=pin_two)
        
        ## @brief    Old tick value to store previous encoder value
        #  @details  Variable used in update function to store previous value
        #
        
        self.old_tick=0
        
        ## @brief    Period value
        #  @details  Variable used to define overflow value for 16 bit integer
        #
        
        self.per=65535
        
        ## @brief    Position value
        #  @details  Variable used to store encoder position
        #
        
        self.position=0

        
    def update(self):
        
        ''' @brief              Updates encoder value
            @details            Updates encoder value by adding delta to position. Handles overflows by adding or subtracting the period from delta when delta exceeds half the period
        '''
        
        ## @brief    new_ticks gets the tick count
        #  @details  Updates tick count at current timer counter
        #
        
        new_tick=self.tim4.counter()
        
        ## @brief    compares new tick to old tick
        #  @details  Compares the newly updated tick to the old tick count to get delta in ticks
        #
        
        self.delta=new_tick-self.old_tick
        self.old_tick=new_tick
        
        if(abs(self.delta) >= self.per/2):    #handle overflow if delta is greater than half the period
            if(self.delta > 0):
                self.delta -= self.per        #if delta is positive, subtract period
            else:
                self.delta += self.per        #if delta is negative, add period

        self.position += self.delta
        
    def get_position(self):
        
        ''' @brief              Gets current encoder position
            @details            Gets current encoder position by returning position variable
            @return             Returns current encoder position
        '''
        
        return self.position
    
    def set_position(self,position):
        
        ''' @brief              Sets position of encoder
            @details            Sets position of encoder by taking input and setting position variable
            @param position     Position to set position variable to       
        '''
        
        self.position = position
        
    def get_delta(self):
        
        ''' @brief              Gets current delta value
            @details            Gets current delta value by returning delta variable
            @return             Returns current delta value
        '''
        return self.delta