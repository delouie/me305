''' @file       ClosedLoop.py
    @brief      Closed loop controller for motor
    @details    Implements a closed loop proportional controller 
    @author     Derick Louie
    @date       November 17, 2021
    @copyright  CC BY
'''

import pyb
import encoder
import time

class ClosedLoop:
    
    ''' @brief     ClosedLoop class
        @details   Implements a proportional controller with methods to get and change Kp and run the controller
    '''
    
    def __init__(self, target, kP, delta_rad):
        
        ''' @brief              Sets up pins PB6 and PB7 for encoder, defines variables
            @details            Sets up encoder pins and defines target, Kp, and delta_rad(shared) variables
        '''
        ## @brief    Creates encoder object
        #  @details  Creates encoder object on pins PB6 and PB7 using channel 4
        #
        
        self.my_encoder = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4, pyb.Timer.ENC_A)
        
        ## @brief    Target value
        #  @details  Target velocity for P controller
        #
        
        self.target = target
        
        ## @brief    Kp constant
        #  @details  Constant that error is multipled by in P controller
        #
        
        self.Kp = kP
        
        ## @brief    Delta radians value
        #  @details  Shared variable to transfer delta value between motor and encoder tasks
        #
        self.delta_rad = delta_rad


        
    def set_Kp(self, value):
        ''' @brief              Sets Kp value
            @details            Writes Kp value to shared variable
        '''
        self.Kp.write(value)
        
    def get_Kp(self):
        ''' @brief              Gets Kp value
            @details            Returns Kp shared variable
            @return             Returns Kp shared variable
        '''
        return self.Kp.read()
            
    def run(self):
        ''' @brief              Runs P controller
            @details            Implements a proportional controller by running the motor at a scaled value corresponding to the error
            @return             Returns PWM value to be passed to motor task
        '''
        self.my_encoder.update()

        ## @brief    Error value
        #  @details  Difference between target velocity and current velocity
        #
        error = float(self.target.read()) - int(self.delta_rad.read())
        
        ## @brief    Speed value
        #  @details  Error value multiplied by Kp constant 
        #
        speed = float(self.Kp.read())*error
        
        ## @brief    PWM value(L)
        #  @details  Scaled speed value to give a PWM signal of 0-100 based off of maximum speed of motor
        #
        pwm=1.754*speed
        if (pwm > 100):
            pwm = 100
        elif (pwm < 0):
            pwm = 0
        #print('error',error)
        #print('pwm',pwm)
        return pwm


if __name__ == '__main__':

    my_loop = ClosedLoop()
    while True:
       my_loop.run(50)
        
