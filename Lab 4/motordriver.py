''' @file       motordriver.py
    @brief      Establishes communication with motors using correct pins
    @details    Can make the motor run in both directions, set the power going into the motor and stop the motor if a fault is triggered.
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 17, 2021
    @copyright  CC BY
'''

import pyb
import utime

class DRV8847:
    
    ''' @brief     Motor Driver
        @details   Works directly with the Nucleo hardware. Initializes sleep and fault conditions to proper pins on the nucleo. Can enable and disable motor. Can also create a Motor object.
    '''
    
    def __init__ (self, faulted):
        
        ''' @brief              Sets up motor pins and corresponding timers
            @details            Sets up sleep and fault pins of the motor driver
            @param faulted      Is true when motor has faulted and prevents motor from being re-enabled
        '''
        
        ## @brief    Assigns pin A15 to an out pin that can sleep the motor
        #  @details  Pin A15 can be set at either .high() to turn the motor on or .low() to disable the motor
        #
        
        self.nSLEEP = pyb.Pin (pyb.Pin.board.PA15, pyb.Pin.OUT_PP)
        
        ## @brief    Initializes fault condition
        #  @details  Faultint is triggered when pin B2 falls from high to low
        #
             
        self.FaultInt = pyb.ExtInt(pyb.Pin.cpu.B2, mode=pyb.ExtInt.IRQ_FALLING,
                   pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        
        ## @brief    Flag for when motor is faulted
        #  @details  Is true when motor has faulted and prevents motor from being re-enabled. Is false once user clears fault condition.
        #  
        
        self.faulted = faulted
    
    def enable (self):
        
        ''' @brief              Enables motor to move
            @details            Disables any previous fault condition and sets nSLEEP to high to enable motor
        '''
        
        self.FaultInt.disable()        # Initially disables fault during enable so previous fault condition does not prevent motor from enabling.
        self.nSLEEP.high()
        utime.sleep_us(25)
        self.FaultInt.enable()
    
    def disable (self):
        
        ''' @brief              Disables motor, preventing movement
            @details            Sets nSLEEP to low to disable motor
        '''
        
        self.nSLEEP.low()              # Motor is disabled when nSLEEP is low
    
    def fault_cb (self, IRQ_src):
        
        ''' @brief              Disables motor when fault condition is triggered
            @details            IRQ_src is triggered when pin B2 falls from high to low
        '''
        
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!FAULT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        self.disable()               
        self.faulted.write(True)
    
    def motor (self):
        
        ''' @brief              Creates and returns a motor object
            @details            Allows for all motor objects to have shared enable/disable/fault properties
        '''
        
        return Motor()
    
    
class Motor:    
    
    ''' @brief     Motor Setup
        @details   Sets up the motor pins for two different motors to move with a specified duty cycle
    '''
    
    def __init__(self):
        
        ''' @brief              Sets up pins and timer to move motor.
            @details            Initializes connection with pins on nucleo, and creates a timer for pwm.
        '''
        
        
        ## @brief    Sets pinB4 to OUT
        #  @details  Establishes one side of the signal to move motor 1
        #
        
        self.pinPB4 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB5 to OUT
        #  @details  Establishes the second side of the signal to move motor 2
        #
        
        self.pinPB5 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB0 to OUT
        #  @details  Establishes one side of the signal to move motor 2
        #
        
        self.pinPB0 = pyb.Pin (pyb.Pin.board.PB0, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB1 to OUT
        #  @details  Establishes the second side of the signal to move motor 2
        #
        
        self.pinPB1 = pyb.Pin (pyb.Pin.board.PB1, pyb.Pin.OUT_PP)
        
        ## @brief    Timer setup
        #  @details  Sets up timer with frequency of 20000 Hz
        #
        
        self.tim3 = pyb.Timer(3, freq=20000)
        
        ## @brief    Channel 1 setup
        #  @details  Configures timer on channel 1 for pin PB4
        #
        
        self.IN1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinPB4)
        
        ## @brief    Channel 2 setup
        #  @details  Configures timer on channel 2 for pin PB5
        #
        
        self.IN2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinPB5)
        
        ## @brief    Channel 3 setup
        #  @details  Configures timer on channel 3 for pin PB0
        #
        
        self.IN3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinPB0)
        
        ## @brief    Channel 4 setup
        #  @details  Configures timer on channel 4 for pin PB1
        #
        
        self.IN4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinPB1)
        
        
    def set_duty (self, duty, enc):
        
        ''' @brief              Accepts a duty cycle percentage and sets it as a pwm to nucleo pins
            @details            Sets the duty to move either encoder 1 or encoder 2 backwards or forwards
            @param              Duty is a percentage of how much power a user wants to run the motor at
            @param              enc is a shared variable that specifies which motor to set duty for
        '''
        
        if (duty > 0):
            
            if(enc.read() == 1):
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(abs(duty)) 
            
            elif(enc.read() == 2):
                self.IN3.pulse_width_percent(0)
                self.IN4.pulse_width_percent(abs(duty)) 
        elif (duty <= 0):
            if(enc.read() == 1):
                self.IN1.pulse_width_percent(abs(duty))
                self.IN2.pulse_width_percent(0) 
                
            if(enc.read() == 2):
                self.IN3.pulse_width_percent(abs(duty))
                self.IN4.pulse_width_percent(0) 
    
if __name__ == '__main__':
    
    print('Running')
    motor_drv = DRV8847()
    motor_1 = motor_drv.motor()
    motor_2 = motor_drv.motor()
    
    motor_drv.enable()
    
    motor_1.set_duty(50)
    motor_2.set_duty(50)
    
    
    