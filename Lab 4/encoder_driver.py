''' 
@file encoder.py
@brief A driver for reading from Quadrature Encoders
@details Allows to use an encoder to determine the movement of a motor. Position changes occur in ticks
         and values are determined from an update function. 
'''

import pyb

class Encoder:
    ''' 
    @brief Interface with quadrature encoders
    @details Class that uses a timer to read from an encoder that is attached to specific pins
    '''    
    def __init__(self, timer, pinA, pinB): 
        
        self.position = 0
        self.tim = pyb.Timer(timer, prescaler = 0, period = 65535)
        
        self.pinA = pyb.Pin(pinA)
        self.pinB = pyb.Pin(pinB)
        
        
        self.timch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin= self.pinA) 
        self.timch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin= self.pinB) 
        self.old = 0


    def update(self):
        '''
        @brief Updates encoder position and delta
        @details the delta value is continuously updated using the difference between time
        '''
        
        self.new = self.tim.counter()
        self.delta = self.new - self.old
        self.old = self.tim.counter()
        
        if(self.delta < -65535/2):
            self.delta += 65536
            
        elif(self.delta > 65535/2):
            self.delta -= 65534

        self.position += self.delta 
            
            
        #print('Reading encoder count and updating position and delta values')

    def get_position(self):
        ''' 
        @brief Returns encoder position
        @details The ticks position is obtained from the encoder whenever this function is called
        @return The position of the encoder shaft
        @should obtain data from the update() method
        '''
        return self.position
    
    def set_position(self, position):
        '''
        @brief Sets encoder position
        @details This function sets the present position of the encoder to any position that is passed through the function.
                 This position is obtained from the update function. 
        @param position The new position of the encoder shaft
        '''

        self.position = position
        self.old = self.tim.counter()
        

    def get_delta(self):
        ''' 
        @brief Returns encoder delta
        @details The delta value is obtained from the update function. 
        @return The change in position of the encoder shaft.
        between the two most recent updates
        '''
        return self.delta      
        