''' @file         task_motor.py
    @brief        Interacts with motordriver to perform motor movement
    @details      Can enable the motor, disable the motor, specify its duty cycle, and clear a fault condition. It performs states depending on what user specifies.
    @author       Jameson Spitz
    @author       Derick Louie
    @date         November 17, 2021
    @copyright    CC BY
'''
import motordriver

class Task_motor():
    
    ''' @brief      Motor task.
        @details    Performs all motor tasks, interacting with motordriver.
    '''
    
    
    def __init__(self, state, user, reset, runs, enc, duty, faulted):
        
        ''' @brief              Constructs motor task task.
            @details            Defines variables and initializes motor task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
            @param enc          A shares.Share object used to specify which motor to perform tasks on
            @param duty         A shares.Share object used to set duty cycle for each motor
            @param faulted      A shares.Share object used to flag if motor has been faulted
        '''
        
        ## @brief      Shared variable to specify which state task encoder runs
        #  @details    Is written by user in task user file
        #
        
        self.my_state = state
        
        ## @brief      Counts the number of runs in task encoder.
        #  @details    Performs some printing and initializing tasks on the first run.
        #
        
        self.runs = runs
        
        ## @brief      Specifies encoder 1 or 2
        #  @details    Tasks are performed on specific encoder depending on the number of enc
        #
        
        self.enc = enc
        
        ## @brief      Specifies how fast to run the motor at
        #  @details    Duty is a percentage of how much power a user wants to run the motor at
        #
        
        self.duty = duty
        
        ## @brief      Writes back to task user to specify state.
        #  @details    Writes back to initial prompting state after a task in encoder is performed.
        #
        
        self.action = user
        
        ## @brief    Flag for when motor is faulted
        #  @details  Is true when motor has faulted and prevents motor from being re-enabled. Is false once user clears fault condition.
        #  
        
        self.faulted = faulted
        
        ## @brief      Creates motor DRV8847 object
        #  @details    Can enable, disable, and fault the motor
        #
        
        self.motor_drv = motordriver.DRV8847(self.faulted)
        
        ## @brief      Creates motor1 object
        #  @details    Can set duty cycle for motor 1
        #
        
        self.motor1 = self.motor_drv.motor()
        
        ## @brief      Creates motor2 object
        #  @details    Can set duty cycle for motor 2
        #
        
        self.motor2 = self.motor_drv.motor()
        
       

        
    def run(self):
        
        ''' @brief              Performs tasks with motordriver methods.
            @details            Can enable motor, disable motor, set duty cycle, and clear a fault condition
        '''
        
        if(self.my_state.read().lower() == 'e'):
            print('Motor Enabled')
            self.motor_drv.enable()
            self.action.write(0)

        if(self.my_state.read().lower() == 'm'):
            if(self.enc.read() == 1):
                print('Setting Duty for Encoder 1:', self.duty.read())
                self.motor1.set_duty(self.duty.read(), self.enc)
                
            elif(self.enc.read() == 2):
                print('Setting Duty for Encoder 2:', self.duty.read())
                self.motor2.set_duty(self.duty.read(), self.enc)
            self.action.write(0)
            
        if(self.my_state.read().lower() == 'n'):
            print('Motor Disabled')
            self.motor_drv.disable()
            self.action.write(0)
        
        if(self.my_state.read().lower() == 'c'):
            self.faulted.write(False)
            self.action.write(0)
                