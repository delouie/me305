''' @file       main.py
    @brief      Main program file 
    @details    Main file runs tasks set up in other files
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 17, 2021
    @copyright  CC BY
'''
# What is left: Fault Condition State, Enabling/Disabling each motor, instantiating encoder 2

import task_encoder
import task_user
import task_motor
import shares

def main():

    ''' @brief              Main function for setting up shared variables and running tasks
        @details            Instantiates shared variables and tasks, then runs each task in a while loop
    '''
    ## @brief    Shared state variable
    #  @details  Variable defining FSM state
    #
    
    state = shares.Share('h')
    
    ## @brief    Shared user_state variable
    #  @details  Variable defining user input
    #
    
    user_state = shares.Share(0)
    
    ## @brief    Shared reset variable
    #  @details  Variable used to reset timers
    #
    
    reset = shares.Share(0)
    
    ## @brief    Shared reset variable
    #  @details  Variable used to print on the first run of the FSM
    #   
    
    runs = shares.Share(0)
    
    ## @brief    Variable to determine which encoder is called
    #  @details  Assigned wither a 1 for encoder 1 or 2 for encoder 2 based off user input
    #   
    
    enc = shares.Share(0)
    
    ## @brief    Variable to duty cycle for each motor
    #  @details  Shared with motordriver to set duty when user specifies duty cycle
    #   
    
    duty = shares.Share(0)
    
    ## @brief    Flag for when motor is faulted
    #  @details  Is true when motor has faulted and prevents motor from being re-enabled. Is false once user clears fault condition.
    #   
    
    faulted = shares.Share(0)
    
    ## @brief    Variable for first task
    #  @details  Instantiates user task to handle all interactions with user
    #   
    
    task1 = task_user.Task_user(state, user_state, reset,runs, enc, duty, faulted)

    ## @brief    Variable for second task
    #  @details  Instantiates encoder task to handle all interaction with encoder driver
    #   
    
    task2 = task_encoder.Task_encoder(state, user_state, reset, runs, enc, duty)
    
    ## @brief    Variable for second task
    #  @details  Instantiates motor task to handle all interaction with motor driver
    #   
    
    task3 = task_motor.Task_motor(state, user_state, reset, runs, enc, duty, faulted)
    
    while(True):                              # Runs the three tasks sequentialy at high frequencies in the while loop
        
            task3.run()
            task2.run()
            task1.run()
    
if __name__ == '__main__':
    main()