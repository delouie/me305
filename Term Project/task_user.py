''' @File         task_user.py
    @brief        User interface for Console and user communication.
    @details      Ask user to specify whether they want to zero motor position, 
                  get motor position, get motor delta, collect data, or end data collection. 
                  Based off user input, a shared state variable is set for task encoder to run the correct action.
    @author       Jameson Spitz
    @author       Derick Louie
    @date         December 8, 2021
    @copyright    CC BY
'''

import time
import pyb

class Task_user():
    
    ''' @brief      User task.
        @details    Reads user inputs and stores in state variables.
    '''
    
    def __init__(self, user_input, x_des, y_des, x_panel, y_panel, x_angle_pitch, y_angle_roll, error_x, error_y):
        
        ''' @brief              Constructs the user task.
            @details            Defines variables and initializes user task
            @param state        A shares.Share object for the FSM state 
            @param user         A shares.Share object for user input
            @param reset        A shares.Share object used to reset timers
            @param runs         A shares.Share object used to print on the first run of the FSM
        '''
        ## @brief     Shared state variable between task user and task encoder.
        #  @details   The state is specified here in task user and used in task encoder to run the action the user specified.

        self.user_input = user_input
        
        self.x_des = x_des
        
        self.y_des = y_des
        
        self.x_panel = x_panel
        
        self.y_panel = y_panel
        
        self.error_x = error_x
        
        self.error_y = error_y
        
        self.x_angle_pitch = x_angle_pitch
        
        self.y_angle_roll = y_angle_roll
        
        self.reset = 1
        
        self.ser = pyb.USB_VCP()
        
        self.set_point_x = []
        self.set_point_y = []
        self.x_position = []
        self.y_position = []
        self.x_theta = []
        self.y_theta = []
        self.x_error = []
        self.y_error = []
        self.timedata = []
        
    def run(self):
        
        ''' @brief              Runs one iteration of FSM
            @details            Reads user inputs and performs actions such as printing to console and writes to the state variable.
        '''
        if (self.user_input.read() == '0'):
            begin = input('\nEnter Key at any time to perform task:\n----------------------------------------------\n1: Begin balancing the ball\n0: Stop Balancing\ng: Callect data\ns: Continue Balancing but end collection\n-----------------------------------------------------\n')
            # self.ball_type = input('Enter weight of ball used: ')
            if(begin == '1'):
                
                print('\n------------------------- Ball is balancing ----------------------\n')
                
                self.user_input.write(1)
                
            elif(begin == 'g'):
                
                self.user_input.write('g')
                
                print('\n-------------- Data collection has begun, but balancing must be started -----------\n')
                
            else:
                print('\n--------------- Enter request again -----------------\n')
                self.user_input.write('0')
                
        if(self.ser.any()):
            
            char_in = self.ser.read().decode()
            
            if(char_in.lower() == 's'):
                self.user_input.write('s')
                
            if(char_in.lower() == 'g'):
                self.user_input.write('g')
                
                print('\n-------------- Data collection has begun -----------\n')
                
            if(char_in == '0'):
                print('\n--------------- Not Balancing. Enter request again -----------------\n')
                self.user_input.write('0')
                
        if (self.user_input.read() == 's'):
            self.set_point_x = []
            self.set_point_y = []
            self.x_position = []
            self.y_position = []
            self.x_theta = []
            self.y_theta = []
            self.timedata = []
            self.x_error = []
            self.y_error = []
            self.user_input.write(1)
            self.reset = 1
            
            print('\n----------------Collection Reset--------------\n')
        
        
        if(self.reset == 1):
            self.starttime=time.ticks_ms()
            self.starttimec=time.ticks_ms()
            self.timeelapsed=0
            self.timeelapsedc=0
        
        if(self.user_input.read() == 'g'):
            
            self.reset = 0
                
            if(self.timeelapsed < 10000):
                
                self.timeelapsed=time.ticks_ms()-self.starttime
                
                self.timeelapsedc=time.ticks_ms()-self.starttimec
            
                if(self.timeelapsedc >= 100):
                        
                    self.set_point_x.append(self.x_des.read())
                    self.set_point_y.append(self.y_des.read())
                    self.x_position.append(self.x_panel.read())
                    self.y_position.append(self.y_panel.read())
                    self.x_theta.append(self.x_angle_pitch.read())
                    self.y_theta.append(self.y_angle_roll.read()) 
                    self.timedata.append(self.timeelapsed)
                    self.x_error.append(self.error_x.read())
                    self.y_error.append(self.error_y.read())
                    self.starttimec=time.ticks_ms()
                     
            else:
                    print('Set point theta_x [deg]\n')
                    print(self.set_point_x, '\n')
                    print('Set point theta_y [deg] \n')
                    print(self.set_point_y, '\n')
                    print('Ball Location x [mm] \n')
                    print(self.x_position, '\n')
                    print('Ball Location y [mm]\n')
                    print(self.y_position, '\n')
                    print('Plate angle theta x [deg] \n')
                    print(self.x_theta, '\n')
                    print('Plate angle theta y [deg] \n')
                    print(self.y_theta, '\n')
                    print('Time data [s] \n')
                    print(self.timedata, '\n')
                    print('Angle error theta x [deg] \n')
                    print(self.x_error, '\n')
                    print('Angle error theta y [deg] \n')
                    print(self.y_error, '\n')
                    
                    self.set_point_x = []
                    self.set_point_y = []
                    self.x_position = []
                    self.y_position = []
                    self.x_theta = []
                    self.y_theta = []
                    self.timedata = []
                    self.x_error = []
                    self.y_error = []
                    self.reset = 1
                    self.user_input.write(1)
            
    
                      