''' @file       motordriver.py
    @brief      Establishes communication with motors using correct pins
    @details    Can make the motor run in both directions, set the power going into the motor and stop the motor if a fault is triggered.
    @author     Jameson Spitz
    @author     Derick Louie
    @date       November 17, 2021
    @copyright  CC BY
'''

import pyb

class Motor:    
    
    ''' @brief     Motor Setup
        @details   Sets up the motor pins for two different motors to move with a specified duty cycle
    '''
    
    def __init__(self):
        
        ''' @brief              Sets up pins and timer to move motor.
            @details            Initializes connection with pins on nucleo, and creates a timer for pwm.
        '''
        
        
        ## @brief    Sets pinB4 to OUT
        #  @details  Establishes one side of the signal to move motor 1
        #
        
        self.pinPB4 = pyb.Pin (pyb.Pin.board.PB4, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB5 to OUT
        #  @details  Establishes the second side of the signal to move motor 2
        #
        
        self.pinPB5 = pyb.Pin (pyb.Pin.board.PB5, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB0 to OUT
        #  @details  Establishes one side of the signal to move motor 2
        #
        
        self.pinPB0 = pyb.Pin (pyb.Pin.board.PB0, pyb.Pin.OUT_PP)
        
        ## @brief    Sets pinB1 to OUT
        #  @details  Establishes the second side of the signal to move motor 2
        #
        
        self.pinPB1 = pyb.Pin (pyb.Pin.board.PB1, pyb.Pin.OUT_PP)
        
        ## @brief    Timer setup
        #  @details  Sets up timer with frequency of 20000 Hz
        #
        
        self.tim3 = pyb.Timer(3, freq=20000)
        
        ## @brief    Channel 1 setup
        #  @details  Configures timer on channel 1 for pin PB4
        #
        
        self.IN1 = self.tim3.channel(1, pyb.Timer.PWM, pin=self.pinPB4)
        
        ## @brief    Channel 2 setup
        #  @details  Configures timer on channel 2 for pin PB5
        #
        
        self.IN2 = self.tim3.channel(2, pyb.Timer.PWM, pin=self.pinPB5)
        
        ## @brief    Channel 3 setup
        #  @details  Configures timer on channel 3 for pin PB0
        #
        
        self.IN3 = self.tim3.channel(3, pyb.Timer.PWM, pin=self.pinPB0)
        
        ## @brief    Channel 4 setup
        #  @details  Configures timer on channel 4 for pin PB1
        #
        
        self.IN4 = self.tim3.channel(4, pyb.Timer.PWM, pin=self.pinPB1)
        
        
    def set_duty (self, duty, motor):
        
        ''' @brief              Accepts a duty cycle percentage and sets it as a pwm to nucleo pins
            @details            Sets the duty to move either encoder 1 or encoder 2 backwards or forwards
            @param              Duty is a percentage of how much power a user wants to run the motor at
            @param              enc is a shared variable that specifies which motor to set duty for
        '''
        
        ## @brief      Max allowed duty cycle value
        #  @details    Sets cap on duty cycle to dampen system, preventing oscilation
        
        max_pwm = 70
        
        ## @brief      Duty cylce offset
        #  @details    Shifts duty cycle by offset to improve time response without increasing individual controller gains
        
        mass_offset = 0
        
        if (duty.read() > 0):                                       # Sets duty cycle for each motor, moving motors in positive direction
            
            if(abs(duty.read() > max_pwm)):                         # Checks if passed duty cylce is greater than cap duty cycle
                duty.write(max_pwm)
            
            if(motor == 'X'):
                self.IN1.pulse_width_percent(0)
                self.IN2.pulse_width_percent(abs(duty.read()) + mass_offset) 
            
            elif(motor == 'Y'):
                self.IN3.pulse_width_percent(abs(duty.read()) + mass_offset)
                self.IN4.pulse_width_percent(0) 
                    
        elif (duty.read() <= 0):                                    # Sets duty cycle for each motor, moving motors in negative direction
            
            if(abs(duty.read() < -max_pwm)):                        # Checks if passed duty cylce is greater than negative cap duty cycle
                duty.write(max_pwm)
                
            if(motor == 'X'):
                self.IN1.pulse_width_percent(abs(duty.read() - mass_offset))
                self.IN2.pulse_width_percent(0) 
                
            if(motor == 'Y'):
                self.IN3.pulse_width_percent(0)
                self.IN4.pulse_width_percent(abs(duty.read() - mass_offset))
    
if __name__ == '__main__':
    '''
    print('Running')
    motor = Motor()
        
    motor.set_duty(-75, 'Y')
    time.sleep(.5)
    motor.set_duty(75, 'Y')
    time.sleep(.5)
    motor.set_duty(0, 'Y')
    
    time.sleep(.5)

    motor.set_duty(-75, 'X')
    time.sleep(.5)
    motor.set_duty(75, 'X')
    time.sleep(.5)
    motor.set_duty(0, 'X')
    '''