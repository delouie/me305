''' @file       task_panel.py
    @brief      Establishes communication with motors using correct pins
    @details    Communicates with IMU driver to extract X and Y pitch and roll angles and add angle offsets for flat position of plate
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 9, 2021
    @copyright  CC BY
'''

import panel_driver
from pyb import Pin
import time

class Task_panel:
        
        def __init__(self, y_des, x_des, x_panel, y_panel):
            
            self.my_panel = panel_driver.Panel_driver(Pin.board.PA0, Pin.board.PA1, Pin.board.PA6, Pin.board.PA7, 176, 100, 88, 50)
            self.y_des = y_des
            self.x_des = x_des
            self.x_direction = 0
            self.x_previous = 0
            self.starttime = time.ticks_ms()
            self.x_des_old = 0
            self.y_des_old = 0
            self.x_panel = x_panel
            self.y_panel = y_panel
        
        def run(self):
            
            
            xyz_position = self.my_panel.scan_xyz()
            
            self.x_panel.write(xyz_position[0])
            self.y_panel.write(xyz_position[1])
            z_scan = xyz_position[2]
            
            self.x_direction = self.x_panel.read() - self.x_previous
            
            #self.x_des.write((-3*10**(-6)*x_panel**3 - 2*10**(-18) + 2*10**(-16) - 4*10**(-14))*1.25)
            
            #self.x_des.write((-.0197*x_panel+1*10**(-17))*1.6)
            
            #self.x_des.write(3*(8*10**(-20)*x_panel**4 - 3*10**(-6)*x_panel**3 + 3*10**(-15)*x_panel**2 -0.0145*x_panel - 5*10**(-12)))
            
            #self.x_des.write(1.2*(4*10**(-6)*x_panel**3 - 2*10**(-17)*x_panel**2 -0.0503*x_panel))
            
            #2.25
            
            outer_gain = 2.75
            
            #3.25
            
            inner_gain_x = 3.4
            
            #1.5
            
            outer_gain_y = 1.75
            
            #1.75
            
            inner_gain_y = 1.75
            
            if(self.x_panel.read() < -30 or self.x_panel.read() > 30):
                #self.x_des.write(1.8*(1*10**(-9)*x_panel**5 + 3*10**(-19)*x_panel**4 - 4*10**(-6)*x_panel**3 - 2*10**(-15)*x_panel**2 - 0.0346*x_panel)) 
                #self.x_des.write(3*(2*10**(-9)*x_panel**5 + 2*10**(-19)*x_panel**4 -1*10**(-5)*x_panel**3+1*10**(-14)*x_panel**2 -.0242*x_panel))
                self.x_des.write(outer_gain*(-3*10**(-9)*(self.x_panel.read()+1)**5 + 6*10**(-19)*(self.x_panel.read()+1)**4 + 1*10**(-5)*(self.x_panel.read()+1)**3 + 2*10**(-14)*(self.x_panel.read()+1)**2 - 0.0341*(self.x_panel.read()+1)))
            elif(self.x_panel.read()<-6 or self.x_panel.read()>6):
                self.x_des.write(inner_gain_x*(2*10**(-5)*(self.x_panel.read()+1)**3 - 7*10**(-18)*(self.x_panel.read()+1)**2 - 0.0426*(self.x_panel.read()+1)))
                #self.x_des.write(3.5*(4*10**(-5)*x_panel**3 + 3*10**(-17)*x_panel**2 - .0441*x_panel))
            else:
                self.x_des.write(0)
                
            if(abs(self.x_des.read()) > .8*outer_gain):
                if(self.x_panel.read() > 0):
                    self.x_des.write(-1.5*outer_gain)
                else:
                    self.x_des.write(1.5*outer_gain)
                    
            #print(self.x_des.read())
            
            #self.x_des.write(1.8*(2*10**(-9)*self.x_panel.read()**5 + 2*10**(-19)*x_panel**4 -1*10**(-5)*x_panel**3+1*10**(-14)*x_panel**2 -.0242*x_panel))
            
            #self.y_des.write(6*(-2*10**(-5)*y_panel**3 -2*10**(-17)*y_panel**2+2*10**(-16)*y_panel))
            
            #self.y_des.write(3*(-5*10**(-9)*y_panel**5 - 3*10**(-18)*y_panel**4 +3*10**(-5)*y_panel**3-1*10**(-14)*self.y_panel.read()**2 -.0552*y_panel))
            
            if(self.y_panel.read() < -18 or self.y_panel.read() > 18):
                #self.x_des.write(1.8*(1*10**(-9)*x_panel**5 + 3*10**(-19)*x_panel**4 - 4*10**(-6)*x_panel**3 - 2*10**(-15)*x_panel**2 - 0.0346*x_panel)) 
                #self.y_des.write(3*(2*10**(-9)*y_panel**5 + 2*10**(-19)*y_panel**4 -1*10**(-5)*y_panel**3+1*10**(-14)*y_panel**2 -.0242*y_panel))
            #elif(y_panel<-4 or y_panel>4):
                #self.y_des.write(2.5*(4*10**(-5)*y_panel**3 + 3*10**(-17)*y_panel**2 - .0441*y_panel))
                self.y_des.write(outer_gain_y*(-5*10**(-5)*(self.y_panel.read()+2.5)**3 + 6*10**(-17)*(self.y_panel.read()+2.5)**2 - 0.0368*(self.y_panel.read()+2.5)))
            else:
                self.y_des.write(inner_gain_y*(9*10**(-5)*(self.y_panel.read()+2.5)**3 + 4*10**(-17)*(self.y_panel.read()+2.5)**2 - 0.0798*(self.y_panel.read()+2.5)))
            #else:
              #  self.y_des.write(0)
                
            if(abs(self.y_des.read()) > 2*outer_gain_y):
                if(self.y_panel.read() > 0):
                    self.y_des.write(-3.2*outer_gain_y)
                else:
                    self.y_des.write(3.2*outer_gain_y)
            
            if(z_scan == False):
                #print('------Not Touching--------')
                self.x_des.write(self.x_des_old)
                self.y_des.write(self.y_des_old)
                
            if(self.x_panel.read() > 82 or self.x_panel.read() < -82):
                self.x_des.write(self.x_des_old)
                
            if(self.y_panel.read() > 39 or self.y_panel.read() < -39):
                self.y_des.write(self.y_des_old)
                
                
            self.x_des_old = self.x_des.read()
            self.y_des_old = self.y_des.read()
            
            #print('Desired y:', self.y_des.read())
            
            #print('Y:',self.y_panel.read())
   
            