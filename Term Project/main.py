''' @file       main.py
    @brief      Runs all necessary tasks in a while loop to continuously update required data
    @details    Runs motor, panel, IMU, and controller tasks to perform closed loop cascade control on platform to balance the ball at the center of the touch panel
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 8th, 2021
    @copyright  CC BY
'''

import task_IMU
import shares
import task_motor
import task_controller
import task_panel
import task_user

def main():
    
    ''' @brief              Initializes all shared variables between tasks and runs all tasks sequentially
        @details            Passes shared variables representing IMU data, desired position, and duty cycle values to relevant tasks. Runs all tasks as quickly as possible.
    '''
    
    ## @brief       Current x angle of platform as shared variable
    #  @details     IMU reading of theta x
    
    x_angle_pitch = shares.Share(0)
    
    ## @brief       Current y angle of platform as shared variable
    #  @details     IMU reading of theta y
    
    y_angle_roll = shares.Share(0)
    
    ## @brief       Flags if IMU is calibrated
    #  @details     Shared with BNO055 driver to change calibrated if calibration check returns false
    
    calibrated = shares.Share(True)
    
    ## @brief       Duty cycle as shared variable
    #  @details     Required value for motor 1 to get the plate to desired x position
    
    pwm_x = shares.Share(0)
    
    ## @brief       Duty cycle as shared variable
    #  @details     Required value for motor 2 to get the plate to desired x position
    
    pwm_y = shares.Share(0)
    
    ## @brief       Desired angle of platform as shared variable
    #  @details     Reresents the desired y position of the ball
    
    y_des = shares.Share(0)
    
    ## @brief       Desired angle of platform as shared variable
    #  @details     Reresents the desired x position of the ball
    
    x_des = shares.Share(0)
    
    error_x = shares.Share(0)
    error_y = shares.Share(0)
    user_input = shares.Share('0')
    x_panel = shares.Share(0)
    y_panel = shares.Share(0)
    
    ## @brief       Proportional Gain
    #  @details     Used to increase response speed and push response towards desired value
    
    kP = 9.5
    
    ## @brief       Integral Gain
    #  @details     Used to increase response speed and eliminate steady state error
    
    kI = 0.9
            
    ## @brief       Derivative Gain
    #  @details     Dampens system, removing oscilations in plate response
    
    kD = 50
    
    '''
    #okay
    kP = 10
    kI = 0.5
    kD = 35
    '''
    '''
    kP = 11.5
    kI = 0.4
    kD = 7
    '''
    '''
    kP = 11
    kI = 0.25
    kD = 15
    '''
    
    '''
    #tues night
    kP = 8
    kI = 0.3
    kD = 1
    '''
    '''
    #rubber ball
    kP = 5.5
    kI = .55
    kD = 5
    '''
    '''
    kP = 16
    kI = 0.03
    kD = 175
    '''
    '''
    kI = 0.035
    kD = 50
    '''
    
    '''
    kP = 12
    kI = 0.03
    kD = 135
    '''
    
    '''
    #first board
    kP = 5
    kI = .06
    kD = 50
    '''
    
            
    ## @brief       Object of IMU task
    #  @details     Handles IMU data and calibration
    
    task1 = task_IMU.Task_IMU(x_angle_pitch, y_angle_roll, calibrated)
    
    ## @brief       Object of controller task
    #  @details     Handles controller calculations
    
    task2 = task_controller.Task_controller(x_angle_pitch, y_angle_roll, y_des, x_des, kP, kI, kD, pwm_x, pwm_y, error_x, error_y, user_input)
    
    ## @brief       Object of motor task
    #  @details     Handles motor activation
    
    task3 = task_motor.Task_motor(pwm_x, pwm_y, user_input)
    
    ## @brief       Object of panel task
    #  @details     Handles panel touch measurements and set point calculation
    
    task4 = task_panel.Task_panel(y_des, x_des, x_panel, y_panel)
    
    task5 = task_user.Task_user(user_input, x_des, y_des, x_panel, y_panel, x_angle_pitch, y_angle_roll, error_x, error_y)

    while(True):                              # Runs the three tasks sequentialy at high frequencies in the while loop
        
            task5.run()
            task1.run()
            task2.run()
            task3.run()
            task4.run()
            #print(pwm_x.read())
            #print(y_angle_roll.read())

if __name__ == '__main__':
    main()