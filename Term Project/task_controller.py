''' @file       task_controller.py
    @brief      Calculates the duty cycle of each motor based on ball position and plate set point
    @details    Controls the position and speed of the plate. Assigns a duty cycle based on the error between the IMU position data and a calculated set angle position. Uses a full PID controller to ensure the plate gets to the desired angle with minimal steady state error and a small settling time. 
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 8th, 2021
    @copyright  CC BY
'''

import time

class Task_controller:
    
    ''' @brief              Returns a duty cycle for each motor
        @details            PID controller adjusts error between desired position and set position in x and y coordinates to calculate proper duty cycle for each motor
    '''
    
    def __init__(self, x_angle_pitch, y_angle_roll, y_des, x_des, kP, kI, kD, pwm_x, pwm_y, error_x, error_y, user_input):
        
        ''' @brief              Initializes all data to calculate error and set PID values
            @details            Initializes IMU euler angle data and desired positions calculated in Task_panel. Controller gains are passed through from main to scale error. The duty cycle variables are also set.
            @param              The IMU euler angle specifying angle of the plate from a flat x axis
            @param              The IMU euler angle specifying angle of the plate from a flat y axis
            @param              Desired angle of the plate in the same plane as y_angle_roll
            @param              Desired angle of the plate in the same plane as x_angle_roll
            @param              Proportional controller gain
            @param              Integral controller gain
            @param              Derivative controller gain
            @param              Duty cycle of motor to move ball along the panel's x plane
            @param              Duty cycle of motor to move ball along the panel's y plane 
        '''
        
        ## @brief       Desired angle of platform
        #  @details     Affects the y position of the ball
        
        self.y_des = y_des
        
        ## @brief       Desired angle of platform
        #  @details     Affects the x position of the ball
        
        self.x_des = x_des
        
        ## @brief       Current angle of platform
        #  @details     IMU reading of theta y
        
        self.y_angle_roll = y_angle_roll
        
        ## @brief       Current angle of platform
        #  @details     IMU reading of theta x
        
        self.x_angle_pitch = x_angle_pitch
        
        ## @brief       Proportional Gain
        #  @details     Used to increase response speed and push response towards desired value
        
        self.kP = kP
        
        ## @brief       Integral Gain
        #  @details     Used to increase response speed and eliminate steady state error
        
        self.kI = kI
        
        ## @brief       Derivative Gain
        #  @details     Dampens system, removing oscilations in plate response
        
        self.kD = kD
        
        ## @brief       Duty cycle
        #  @details     Required value for motor 1 to get the plate to desired x position
        
        self.pwm_x = pwm_x
        
        ## @brief       Duty cycle
        #  @details     Required value for motor 2 to get the plate to desired y position
        
        self.pwm_y = pwm_y
        
        ## @brief       Integral Error
        #  @details     Variable stores the total error in x direction over a given amount of time for intrgral control
        
        self.integral_x = 0
        
        ## @brief       Integral Error
        #  @details     Variable stores the total error in y direction over a given amount of time for integral control
        
        self.integral_y = 0
        
        ## @brief       Previous (n-1) error value
        #  @details     Stores the last error value to caluculate the change in error for derivative control
        
        self.prev_error_x = 0
        
        ## @brief       Previous (n-1) error value
        #  @details     Stores the previous error value to caluculate the change in error for derivative control
        
        self.prev_error_y = 0
    
        self.error_y = error_y
        
        self.error_x = error_x
        
        self.user_input = user_input
    
    def run(self):
        
        ''' @brief              Calculates a new duty cycle for each motor every time this method is ran
            @details            Uses current IMU and desired position data to find a duty cycle with a PID controller
        '''
        
        ## @brief       Angle position error
        #  @details     Stores differnce between desired position and actual position in x direction
        
        if(self.user_input.read() == 1 or self.user_input.read() == 'g' or self.user_input.read() == 's'):
        
            self.error_x.write(self.x_des.read() - self.x_angle_pitch.read())
            
            #print('-----------',self.error_y.read(), '----------------')
            
            
            '''
            if(error_x >= -0.375 and error_x <= 0.375):
                self.kI_temp = self.kI*2
            else:
                self.kI_temp = self.kI/2
            '''
            
            # Stores total error over unlimited consecutive runs of method in x direction
            
            self.integral_x += self.error_x.read()
            
            '''
            if (error_x <= .2 and error_x >= -.2):
                self.integral_x = 0
            
            
            if (error_x > 2.5 or error_x < -2.5):
                self.integral_x = 0
            '''
            
            ## @brief       Derivative error
            #  @details     Stores difference beteen the current position error and precious position error to find the change in error in the x direction
            
            self.derivative_x = (self.error_x.read() - self.prev_error_x)
            
            # Writes pwm for motor 1 in terms from KP, KI, and KD multiplied by their corresonding error values
            
            self.pwm_x.write(self.kP * self.error_x.read() + self.kI * self.integral_x + self.kD * self.derivative_x)
            
            ## @brief       Angle position error
            #  @details     Stores differnce between desired position and actual position in y direction
            
            self.error_y.write(self.y_des.read() - self.y_angle_roll.read())
            
            # Stores total error over unlimited consecutive runs of method in y direction
            
            self.integral_y += self.error_y.read()
            
            ## @brief       Derivative error
            #  @details     Stores difference beteen the current position error and precious position error to find the change in error in the y direction
            
            self.derivative_y = (self.error_y.read() - self.prev_error_y)
            
            # Writes pwm for motor 2 in terms from KP, KI, and KD multiplied by their corresonding error values
            
            self.pwm_y.write(1*self.kP * self.error_y.read() + 1.6*self.kI * self.integral_y + self.kD * self.derivative_y)
            
            #time.sleep_ms(dT)
            
            # Stores current error to be used as previous error in next run
            
            self.prev_error_x = self.error_x.read()
            self.prev_error_y = self.error_y.read()
            
            print('P:', self.kP*self.error_x.read(), 'I:', self.kI*self.integral_x, 'D:',self.derivative_x*self.kD, 'PWM:', self.pwm_x.read(), 'Error:', self.error_x.read())
            print('P:', self.kP*self.error_y.read(), 'I:', self.kI*self.integral_y, 'D:',self.derivative_y*self.kD, 'PWM:', self.pwm_y.read(), 'Error:', self.error_y.read())
            
            