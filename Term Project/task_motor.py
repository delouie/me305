''' @file       task_motor.py
    @brief      Establishes communication with motors
    @details    Sets duty cycle for each motor.
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 8, 2021
    @copyright  CC BY
'''

import motor_driver

class Task_motor():
    
    ''' @brief              Passes duty cycle through to each motor
        @details            Duty cycle is calculated in controller task, and passed through to task_motor to set the duty cycle to each motor
    '''
    
    def __init__(self, pwm_x, pwm_y, user_input):
        
        ''' 
        @brief              Instantiates motor objects and creates variables
        @details            Creates necessary objects and variables for use in run method
        @param              PWM value for motor that controls platform in X direction
        @param              PWM value for motor that controls platform in Y direction
        @param              User input from user task
        '''
        
        ## @brief       Motor object instantiated from motor class
        #  @detail      Respresents motor controling the ball's x location
        
        self.motor_x = motor_driver.Motor()
        
        ## @brief       Motor object instantiated from motor class
        #  @detail      Respresents motor controling the ball's y location
        
        self.motor_y = motor_driver.Motor()
        
        ## @brief       Duty cycle as shared variable
        #  @details     Required duty cycle for motor 1 to get the plate to desired x position
        
        self.pwm_x = pwm_x
        
        ## @brief       Duty cycle as shared variable
        #  @details     Required duty cycle for motor 2 to get the plate to desired y position
        
        self.pwm_y = pwm_y
        
        ## @brief       User input shared variable
        #  @details     Shared variable for user input from user task
        
        self.user_input = user_input
        
        
    def run(self):
        
        if(self.user_input.read() == 1 or self.user_input.read() == 'g' or self.user_input.read() == 's'):
        
            self.motor_x.set_duty(self.pwm_x, 'X')         # Communicates with motor driver to set duty cycle
            self.motor_y.set_duty(self.pwm_y, 'Y')
        
        