''' @file       Task_IMU.py
    @brief      Establishes communication with motors using correct pins
    @details    Communicates with IMU driver to extract X and Y pitch and roll angles and add angle offsets for flat position of plate
    @author     Jameson Spitz
    @author     Derick Louie
    @date       December 9, 2021
    @copyright  CC BY
'''

import BNO055

class Task_IMU:
    
    ''' @brief              IMU task
        @details            Sets up IMU and writes pitch and roll angles to shared variables for use in panel task
    '''
    
    def __init__(self, x_pos, y_pos, calibrated):
        
        ''' 
        @brief              Instantiates IMU object, set operating mode and creates variables
        @details            Creates necessary objects and variables for use in run method
        @param              The angle(in degrees) of the platform in the X axis
        @param              The angle(in degrees) of the platform in the Y axis
        @param              Boolean that indicates if IMU is calibrated
        '''
        
        ## @brief       IMU object
        #  @details     IMU object created from BNO055 file
        #
        
        self.my_imu = BNO055.BNO055()
        
        self.my_imu.operating_mode(12)
        
        ## @brief       Flags if IMU is calibrated
        #  @details     Shared with BNO055 driver to change calibrated if calibration check returns false
        #
        
        self.calibrated = self.my_imu.calib_status()
        
        ## @brief       Current x angle of platform as shared variable
        #  @details     IMU reading of theta x
        #
        
        self.x_angle_pitch = x_pos
        
        ## @brief       Current y angle of platform as shared variable
        #  @details     IMU reading of theta y
        #
        
        self.y_angle_roll = y_pos
        
    def run(self):
        
        ''' 
        @brief              Writes current panel x and y angle to shared variable
        @details            Writes euler angles and implements offsets for flat position of platform
        '''
        
        ## @brief       Euler angles
        #  @details     Stores pitch, roll, and heading from IMU
        
        euler_angles = self.my_imu.euler_angle(self.calibrated)
        
        self.x_angle_pitch.write(euler_angles[2] - 0.25)
        self.y_angle_roll.write(euler_angles[1] + 1.75)
                

    
        
        
        
        
        

        
        
        